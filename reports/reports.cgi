#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
require "globals.pl";

print "Content-type: text/html\n\n";

PutHeader ("Reports");

print <<FIN;
<h2>Generic GNOME reports</h2>
<ul>
<li><a href="weekly-bug-summary.html">Summary of bug activity for the last week</a>.</li>
<!--
<li><a href="product-keyword-report.cgi">Open bugs listed by product and keyword.</a>.
<li><a href="product-target-report.cgi">Open bugs listed by product and target.</a>.
-->
<li><a href="mostfrequent.html">Most frequently reported bugs for all of Gnome Bugzilla.</a><br>
<li><a href="product-mostfrequent.cgi">Most frequently reported bugs by product.</a><br>
<li><a href="unconfirmed-bugs.cgi">Show products with the most UNCONFIRMED bugs. Good starting point for new bughunters looking to triage bugs.</a>.<br>
<li><a href="needinfo-updated.cgi">Show the NEEDINFO reports which have been updated (e.g. given new info)</a>.<br>
<li><a href="../describekeywords.cgi">Show the different keywords available to Gnome bugzilla and what they mean.</a>.<br>
</ul>
<h2>Reports Specific to the GNOME 2.x effort</h2>
<ul>
<li><a href="core-bugs-today.cgi">Bugs filed today</a> against components listed at <a href="http://www.gnome.org/start/unstable/modules/">gnome.org/start/</a> as core GNOME components.</li>
<li> Open bugs for specific GNOME versions, broken down by product, component, and developer:
  <ul>
    <li><a href="gnome-20-report.html">GNOME 2.0</a> </li>
    <li><a href="gnome-22-report.html">GNOME 2.1/2.2</a> </li>
    <li><a href="gnome-24-report.html">GNOME 2.3/2.4</a> </li>
    <li><a href="gnome-26-report.html">GNOME 2.5/2.6</a> </li>
    <li><a href="gnome-28-report.html">GNOME 2.7/2.8</a> </li>
  </ul>
</li>
<li> Summary of bug activity for the last week, by GNOME version:
  <ul>
    <li> <a href="weekly-bug-summary-gnome20.html">GNOME 2.0</a> </li>
    <li> <a href="weekly-bug-summary-gnome22.html">GNOME 2.1/2.2</a> </li>
    <li> <a href="weekly-bug-summary-gnome24.html">GNOME 2.3/2.4</a> </li>
    <li> <a href="weekly-bug-summary-gnome26.html">GNOME 2.5/2.6</a> </li>
    <li> <a href="weekly-bug-summary-gnome28.html">GNOME 2.7/2.8</a> </li>
  </ul>
</li>
</ul>
If you have questions or suggestions for new reports, email <a href="mailto:bugmaster\@gnome.org">the GNOME bugmasters.</a><br><br>
FIN
PutFooter();

exit;

