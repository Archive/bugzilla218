#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.

#This is a simple report that runs an SQL query and returns the data
#by piping it to the bug list cgi. Note that with the upgrade to 2.16
#the query currently in the example SQL [which does a boolean search
#on keywords] can now be done from the standard query interface.

use diagnostics;
use strict;

#this is required since this script now lives in the reports/ subdir 
push @INC, "../.";

#provides some useful data including database information.
require "globals.pl";

#hooks us up to the database
ConnectToDatabase(1);

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

#here we have an arbitrary SQL query
my $query = <<FIN;
select 
    distinct(bugs.bug_id)
from   bugs
where
    (
     (
      bugs.product = 'GConf' or
      bugs.product = 'audiofile' or
      bugs.product = 'bonobo-activation [was: oaf]' or
      bugs.product = 'esound' or
      bugs.product = 'gnome-mime-data' or
      bugs.product = 'gnome-vfs' or
      bugs.product = 'linc' or
      bugs.product = 'bug-buddy' or
      bugs.product = 'eel' or
      bugs.product = 'EOG' or
      bugs.product = 'gdm' or
      bugs.product = 'gedit' or
      bugs.product = 'control-center' or
      bugs.product = 'gnome-games' or
      bugs.product = 'gnome-media' or
      bugs.product = 'system-monitor [was: procman]' or
      bugs.product = 'gnome-user-docs' or
      bugs.product = 'libgtop' or
      bugs.product = 'metacity' or
      bugs.product = 'nautilus' or
      bugs.product = 'gnome-panel' or
      bugs.product = 'gnome-terminal' or
      bugs.product = 'gnome-desktop' or
      bugs.product = 'printman' or
      bugs.product = 'yelp'
      )
     or
     (
      bugs.component = 'libbonoboui' or
      bugs.component = 'libbonobo' or
      bugs.component = 'gnome-about' or
      bugs.component = 'gnome-terminal' or
      bugs.component = 'gsm' or
      bugs.component = 'panel' or
      bugs.component = 'cdplayer' or
      bugs.component = 'charpick' or
      bugs.component = 'deskguide' or
      bugs.component = 'gtik' or
      bugs.component = 'gweather' or
      bugs.component = 'mailcheck' or
      bugs.component = 'mini-commander' or
      bugs.component = 'mixer' or
      bugs.component = 'modemlights' or
      bugs.component = 'odometer' or
      bugs.component = 'screenshooter' or
      bugs.component = 'tasklist' or
      bugs.component = 'gcalc' or
      bugs.component = 'gcharmap' or
      bugs.component = 'gcolorsel' or
      bugs.component = 'gdialog' or
      bugs.component = 'gfontsel' or
      bugs.component = 'gsearchtool' or
      bugs.component = 'logview' or
      bugs.component = 'meat-grinder' or
      bugs.component = 'stripchart' or
      bugs.component = 'gfloppy'
      )
     )
and
    bugs.keywords LIKE '%GNOME2%'
and
    bugs.keywords NOT LIKE '%triaged%'
and
    (
     bugs.bug_status = 'UNCONFIRMED' or 
     bugs.bug_status = 'NEW' or 
     bugs.bug_status = 'ASSIGNED' or 
     bugs.bug_status = 'REOPENED'
     )
FIN

# End build up $query string and submit it to the DB
    SendSQL ($query);

# here we do a crappy join on the returned data
my $buglist;
    while (my ($bug_id) = FetchSQLData()) {
        $buglist .= $bug_id;
        $buglist .= ',';
    }

# we do a redirect to the relevant list
print<<FIN;
<HEAD>
<meta http-equiv="refresh"content="0; url=http://bugzilla.gnome.org/buglist.cgi?bug_id=$buglist">
</HEAD>
FIN
exit;





