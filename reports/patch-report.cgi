#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# unconfirmed-bugs.cgi
#
# Display the product-components with the most unconfirmed bugs.
# A good place for bug hunters to start triaging.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#


use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=bugzilla_report.html\n\n";

ConnectToDatabase(1);

PutHeader("Gnome Bugzilla - Unreviewed Patches");

# Always spew informative messages.
print "<p>Here is a list of GNOME Bugzilla products with the most unreviewed patches. Maintainers can hopefully use this page to help track and review new patches that come in to bugzilla.";

print "<p> NOTE: the count in this table is the number of patches; the link goes to the bugs, which is basically always going to be a smaller number. Oh, and there must be a generic way to escape the pluses in gtk+, but I haven't done that yet.";

&print_unconfirmed_bug_list();

print "<p> If you spot any errors in this page please report it to the bugzilla component of bugzilla.gnome.org. Thanks.</p>";

PutFooter();

sub print_unconfirmed_bug_list() {
	my($number) = @_;

my $query = <<FIN;
SELECT bugs.product, bugs.component, count(attachments.bug_id) as n FROM attachments LEFT JOIN attachstatuses ON attachments.attach_id=attachstatuses.attach_id LEFT JOIN bugs ON attachments.bug_id=bugs.bug_id WHERE attachstatuses.statusid IS NULL and attachments.ispatch='1' and attachments.isobsolete!='1' and (bugs.bug_status='UNCONFIRMED' or bugs.bug_status='NEW' or bugs.bug_status='ASSIGNED' or bugs.bug_status='REOPENED') GROUP BY bugs.product, bugs.component ORDER BY n DESC
FIN

        my $product;
        my $component;
        my $count;

	# Print a nice cross-referenced table of results.
	print "<table border=1 cellspacing=0 cellpadding=5>\n";
	print "<tr><th>Product</th><th>Component</th><th>unreviewed patches</th></tr>\n";
	SendSQL ($query);
	while (my ($product, $component, $count) = FetchSQLData()) {

print <<FIN;
        <tr><td>$product</td>
	<td><a href="../patch-status.cgi?product=$product&component=$component">$component</a></td>
	<td>$count</td>
	</tr>
FIN

	}
	print "</table><p>\n";

}
