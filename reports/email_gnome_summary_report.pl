#!/usr/bonsaitools/bin/perl -w
#
# email_gnome_summary_report.pl
#
# Send XML'ed weekly summary stats to the weekly gnome-summary ppl.
#
# Author: Wayne Schuller (k_wayne@linuxpower.org)
#
# TODO:
#

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "globals.pl";
require "weekly-summary-utils.pl";

my $from = "k_wayne\@linuxpower.org";
my $to = "k_wayne\@linuxpower.org, gnome-summary\@gnome.org";
my $subject = "Weekly Gnome Bug Activity Report";
my $days = 7; # Change this if the definition of week changes. 
my $product = "%"; # This report is for all products.
my $gnome2_keyword = "%"; # Don't limit to Gnome2 keyword.
my $number = 15;	# Show the top 15.

my $bugs_open = &print_total_bugs_on_bugzilla($gnome2_keyword);
my $bugs_opened = &bugs_opened($product, $days, $gnome2_keyword);
my $bugs_closed = &bugs_closed($product, $days, $gnome2_keyword);

open SENDMAIL, "|/usr/lib/sendmail -t";
print SENDMAIL "From: $from\n";
print SENDMAIL "To: $to\n";
print SENDMAIL "Subject: $subject\n";
print SENDMAIL "<bug-stats>\n";
print SENDMAIL "<bug-modules open='$bugs_open' opened='$bugs_opened' closed='$bugs_closed'>\n";

# Passing the filehandle isn't tested well.
&print_product_bug_lists($number, $days, "XML", \*SENDMAIL, $gnome2_keyword);

&print_bug_hunters_list($number, $days, "XML", \*SENDMAIL, $gnome2_keyword);

print SENDMAIL "</bug-stats>\n";

close SENDMAIL;
