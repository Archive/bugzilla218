# -*- Mode: perl; indent-tabs-mode: nil -*-

use diagnostics;
use strict;

sub gnome_find_version {
	my ($gnomestring) = @_;
	
	if ($gnomestring =~ /GNOME2\.0/) {
		return "2.0";
	} elsif ($gnomestring =~ /GNOME2\.[12]/) {
		return "2.1/2.2";
	} elsif ($gnomestring =~ /GNOME2\.[34]/) {
		return "2.3/2.4";
	} elsif ($gnomestring =~ /GNOME2\.[56]/) {
		return "2.5/2.6";
	} elsif ($gnomestring =~ /GNOME2\.[78]/) {
		return "2.7/2.8";
	} else {
		return "Unspecified";
	}
}

1;
