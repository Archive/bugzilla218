CONFLICTS
#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>
#                 Dan Mosedale <dmose@mozilla.org>
#                 Stephan Niemz  <st.n@gmx.net>
#                 Andreas Franke <afranke@mathweb.org>
#                 Myk Melez <myk@mozilla.org>

################################################################################
# Script Initialization
################################################################################

# Make it harder for us to do dangerous things in Perl.
use strict;

use lib qw(.);

use vars qw($template $vars);

use Bugzilla;
use Bugzilla::Search;

# Include the Bugzilla CGI and general utility library.
require "CGI.pl";

<<<<<<< buglist.cgi
# Shut up misguided -w warnings about "used only once".  "use vars" just
# doesn't work for me.
sub sillyness {
    my $zz;
    $zz = $::db_name;
    $zz = @::components;
    $zz = @::default_column_list;
    $zz = $::defaultqueryname;
    $zz = @::dontchange;
    $zz = @::legal_keywords;
    $zz = @::legal_platform;
    $zz = @::legal_priority;
    $zz = @::legal_product;
    $zz = @::legal_severity;
    $zz = @::settable_resolution;
    $zz = @::target_milestone;
    $zz = $::legal_customfields;
    $zz = $::unconfirmedstate;
    $zz = $::userid;
    $zz = @::versions;
};
=======
use vars qw($db_name
            @components
            @default_column_list
            $defaultqueryname
            @legal_keywords
            @legal_platform
            @legal_priority
            @legal_product
            @legal_severity
            @settable_resolution
            @target_milestone
            $unconfirmedstate
            $userid
            @versions);

my $cgi = Bugzilla->cgi;

if (length($::buffer) == 0) {
    print $cgi->header(-refresh=> '10; URL=query.cgi');
    ThrowUserError("buglist_parameters_required");
}
>>>>>>> 1.1.1.2

ConnectToDatabase();
GetVersionTable();


################################################################################
# Data and Security Validation
################################################################################

# Whether or not the user wants to change multiple bugs.
my $dotweak = $::FORM{'tweak'} ? 1 : 0;

# Log the user in
if ($dotweak) {
    confirm_login();
    UserInGroup("editbugs") || ThrowUserError("insufficient_privs_for_multi");
    GetVersionTable();
}
else {
    quietly_check_login();
}

# Hack to support legacy applications that think the RDF ctype is at format=rdf.
if ($::FORM{'format'} && $::FORM{'format'} eq "rdf" && !$::FORM{'ctype'}) { 
    $::FORM{'ctype'} = "rdf";
    delete($::FORM{'format'});
}

# The js ctype presents a security risk; a malicious site could use it  
# to gather information about secure bugs. So, we only allow public bugs to be
# retrieved with this format.
#
# Note that if and when this call clears cookies or has other persistent 
# effects, we'll need to do this another way instead.
if ((exists $::FORM{'ctype'}) && ($::FORM{'ctype'} eq "js")) {
    Bugzilla->logout_request();
}

# Determine the format in which the user would like to receive the output.
# Uses the default format if the user did not specify an output format;
# otherwise validates the user's choice against the list of available formats.
my $format = GetFormat("list/list", $::FORM{'format'}, $::FORM{'ctype'});

# Use server push to display a "Please wait..." message for the user while
# executing their query if their browser supports it and they are viewing
# the bug list as HTML and they have not disabled it by adding &serverpush=0
# to the URL.
#
# Server push is a Netscape 3+ hack incompatible with MSIE, Lynx, and others. 
# Even Communicator 4.51 has bugs with it, especially during page reload.
# http://www.browsercaps.org used as source of compatible browsers.
#
my $serverpush =
  $format->{'extension'} eq "html"
    && exists $ENV{'HTTP_USER_AGENT'} 
      && $ENV{'HTTP_USER_AGENT'} =~ /Mozilla.[3-9]/ 
        && $ENV{'HTTP_USER_AGENT'} !~ /[Cc]ompatible/
          && $ENV{'HTTP_USER_AGENT'} !~ /WebKit/
            && !defined($::FORM{'serverpush'})
              || $::FORM{'serverpush'};

my $order = $::FORM{'order'} || "";
my $order_from_cookie = 0;  # True if $order set using $::COOKIE{'LASTORDER'}

# The params object to use for the actual query itself
my $params;

# If the user is retrieving the last bug list they looked at, hack the buffer
# storing the query string so that it looks like a query retrieving those bugs.
if ($::FORM{'regetlastlist'}) {
    $::COOKIE{'BUGLIST'} || ThrowUserError("missing_cookie");

    $order = "reuse last sort" unless $order;

    # set up the params for this new query
    $params = new Bugzilla::CGI({
                                 bug_id => [split(/:/, $::COOKIE{'BUGLIST'})],
                                 order => $order,
                                });
}

if ($::buffer =~ /&cmd-/) {
    my $url = "query.cgi?$::buffer#chart";
    print $cgi->redirect(-location => $url);
    # Generate and return the UI (HTML page) from the appropriate template.
    $vars->{'message'} = "buglist_adding_field";
    $vars->{'url'} = $url;
    $template->process("global/message.html.tmpl", $vars)
      || ThrowTemplateError($template->error());
    exit;
}

################################################################################
# Utilities
################################################################################

<<<<<<< buglist.cgi
sub SqlifyDate {
    my ($str) = @_;
    $str = time2str("%Y-%m-%d %H:%M:%S", time()) unless $str;
    my $date = str2time($str);
    if (!defined($date)) {
        my $htmlstr = html_quote($str);
        DisplayError("The string <tt>$htmlstr</tt> is not a legal date.");
        exit;
    }
    return time2str("%Y-%m-%d %H:%M:%S", $date);
}

=======
>>>>>>> 1.1.1.2
my @weekday= qw( Sun Mon Tue Wed Thu Fri Sat );
sub DiffDate {
    my ($datestr) = @_;
    my $date = str2time($datestr);
    my $age = time() - $date;
    my ($s,$m,$h,$d,$mo,$y,$wd)= localtime $date;
    if( $age < 18*60*60 ) {
        $date = sprintf "%02d:%02d:%02d", $h,$m,$s;
    } elsif( $age < 6*24*60*60 ) {
        $date = sprintf "%s %02d:%02d", $weekday[$wd],$h,$m;
    } else {
        $date = sprintf "%04d-%02d-%02d", 1900+$y,$mo+1,$d;
    }
    return $date;
}

sub LookupNamedQuery {
    my ($name) = @_;
    confirm_login();
    my $userid = DBNameToIdAndCheck($::COOKIE{"Bugzilla_login"});
    my $qname = SqlQuote($name);
    SendSQL("SELECT query FROM namedqueries WHERE userid = $userid AND name = $qname");
    my $result = FetchOneColumn();
    $result || ThrowUserError("missing_query", {'queryname' => $name});
    return $result;
}

sub LookupSeries {
    my ($series_id) = @_;
    detaint_natural($series_id) || ThrowCodeError("invalid_series_id");
    
    my $dbh = Bugzilla->dbh;
    my $result = $dbh->selectrow_array("SELECT query FROM series " .
                                       "WHERE series_id = $series_id");
    $result
           || ThrowCodeError("invalid_series_id", {'series_id' => $series_id});
    return $result;
}

sub GetQuip {

    my $quip;

    # COUNT is quick because it is cached for MySQL. We may want to revisit
    # this when we support other databases.

    SendSQL("SELECT COUNT(quip) FROM quips WHERE approved = 1");
    my $count = FetchOneColumn();
    my $random = int(rand($count));
    SendSQL("SELECT quip FROM quips WHERE approved = 1 LIMIT $random,1");

    if (MoreSQLData()) {
        ($quip) = FetchSQLData();
    }

    return $quip;
}

sub GetGroupsByUserId {
    my ($userid) = @_;

    return if !$userid;

    SendSQL("
        SELECT DISTINCT  groups.id, name, description, isactive
                   FROM  groups, user_group_map
                  WHERE  user_id = $userid AND NOT isbless
                    AND  user_group_map.group_id = groups.id
                    AND  isbuggroup
               ORDER BY  description ");

    my @groups;

    while (MoreSQLData()) {
        my $group = {};
        ($group->{'id'}, $group->{'name'},
         $group->{'description'}, $group->{'isactive'}) = FetchSQLData();
        push(@groups, $group);
    }

    return \@groups;
}


################################################################################
# Command Execution
################################################################################

<<<<<<< buglist.cgi
sub CustomFieldNumbers {
    my (@fieldnames) = @_;
    my $res = join (', ', map {$::legal_customfields{$_}[2]} @fieldnames);
    return $res;
}

sub GenerateSQL {
    my $debug = 0;
    my ($fieldsref, $supptablesref, $wherepartref, $urlstr, $custom_cols) = (@_);
    my @fields;
    my @supptables = @$supptablesref if $supptablesref;
    my @wherepart = @$wherepartref if $wherepartref;
    my @custom_columns;   
    @fields = @$fieldsref if $fieldsref;
    @custom_columns = @$custom_cols if $custom_cols;
    my %F;
    my %M;
    ParseUrlString($urlstr, \%F, \%M);
    my @specialchart;
    my @andlist;

    # First, deal with all the old hard-coded non-chart-based poop.

    unshift(@supptables,
            ("profiles map_assigned_to",
             "profiles map_reporter",
             "LEFT JOIN profiles map_qa_contact ON bugs.qa_contact = map_qa_contact.userid"));
    unshift(@wherepart,
            ("bugs.assigned_to = map_assigned_to.userid",
             "bugs.reporter = map_reporter.userid"));

# customfields

     if (@custom_columns > 0) {
         #push(@fields, "bugs_customfields.cf_id ");
         push(@fields, "customfields.name");
         push(@fields, "bugs_customfields.value ");
         push(@supptables, "LEFT JOIN bugs_customfields ".
              "ON bugs_customfields.bug_id = bugs.bug_id");
         push(@supptables, "LEFT JOIN customfields ".
              "ON customfields.id = bugs_customfields.cf_id");            
         # push(@supptables,"ON bugs_customfields.bug_id = bugs.bug_id");  
         my $where =  "(bugs_customfields.cf_id " . 
             "IN (" . CustomFieldNumbers(@custom_columns) . ") OR " .
             " bugs_customfields.cf_id IS NULL)" .
             " AND customfields.id = bugs_customfields.cf_id ";
         push(@wherepart, $where);
     }

    my $minvotes;
    if (defined $F{'votes'}) {
        my $c = trim($F{'votes'});
        if ($c ne "") {
            if ($c !~ /^[0-9]*$/) {
                my $htmlc = html_quote($c);
                DisplayError("The <em>At least ___ votes</em> field must be
                              a simple number.  You entered <kbd>$htmlc</kbd>,
                              which doesn't cut it.");
                exit;
            }
            push(@specialchart, ["votes", "greaterthan", $c - 1]);
        }
    }
=======
$::FORM{'cmdtype'} ||= "";
$::FORM{'remaction'} ||= "";
>>>>>>> 1.1.1.2

# Backwards-compatibility - the old interface had cmdtype="runnamed" to run
# a named command, and we can't break this because it's in bookmarks.
if ($::FORM{'cmdtype'} eq "runnamed") {  
    $::FORM{'cmdtype'} = "dorem"; 
    $::FORM{'remaction'} = "run";
}

<<<<<<< buglist.cgi
# This is evil.  We should never allow a user to directly append SQL to
# any query without a huge amount of validation.  Even then, it would
# be a bad idea.  Beware that uncommenting this will allow someone to
# peak at virtually anything they want in the bugs database.
#    if (defined $F{'sql'}) {
#        die "Invalid sql: $F{'sql'}" if $F{'sql'} =~ /;/;
#        push(@wherepart, "( $F{'sql'} )");
#    }

    my @legal_fields = ("product", "version", "rep_platform", "op_sys",
                        "bug_status", "resolution", "priority", "bug_severity",
                        "assigned_to", "reporter", "component",
                        "target_milestone", "groupset", "gg_version", "gg_milestone");

    foreach my $field (keys %F) {
        if (lsearch(\@legal_fields, $field) != -1) {
            push(@specialchart, [$field, "anyexact",
                                 join(',', @{$M{$field}})]);
        }
    }
=======
# Now we're going to be running, so ensure that the params object is set up,
# using ||= so that we only do so if someone hasn't overridden this 
# earlier, for example by setting up a named query search.
>>>>>>> 1.1.1.2

# This will be modified, so make a copy.
$params ||= new Bugzilla::CGI($cgi);

<<<<<<< buglist.cgi
    foreach my $id ("1", "2") {
        if (!defined ($F{"email$id"})) {
            next;
        }
        my $email = trim($F{"email$id"});
        if ($email eq "") {
            next;
        }
        my $type = $F{"emailtype$id"};
        if ($type eq "exact") {
            $type = "anyexact";
            foreach my $name (split(',', $email)) {
                $name = trim($name);
                if ($name) {
                    DBNameToIdAndCheck($name);
                }
            }
        }

        my @clist;
        foreach my $field ("assigned_to", "reporter", "cc", "qa_contact") {
            if ($F{"email$field$id"}) {
                push(@clist, $field, $type, $email);
            }
        }
        if ($F{"emaillongdesc$id"}) {
            my $table = "longdescs_";
            push(@supptables, "longdescs $table");
            push(@wherepart, "$table.bug_id = bugs.bug_id");
            my $ptable = "longdescnames_";
            push(@supptables, "profiles $ptable");
            push(@wherepart, "$table.who = $ptable.userid");
            push(@clist, "$ptable.login_name", $type, $email);
        }
        if (@clist) {
            push(@specialchart, \@clist);
        } else {
            my $htmlemail = html_quote($email);
            DisplayError("You must specify one or more fields in which
                          to search for <tt>$htmlemail</tt>.");
            exit;
        }
    }


    if (defined $F{'changedin'}) {
        my $c = trim($F{'changedin'});
        if ($c ne "") {
            if ($c !~ /^[0-9]*$/) {
                my $htmlc = html_quote($c);
                DisplayError("The <em>changed in last ___ days</em> field
                              must be a simple number.  You entered
                              <kbd>$htmlc</kbd>, which doesn't cut it.");
                exit;
            }
            push(@specialchart, ["changedin",
                                 "lessthan", $c + 1]);
        }
    }

    my $ref = $M{'chfield'};

    if (defined $ref) {
        my $which = lsearch($ref, "[Bug creation]");
        if ($which >= 0) {
            splice(@$ref, $which, 1);
            push(@specialchart, ["creation_ts", "greaterthan",
                                 SqlifyDate($F{'chfieldfrom'})]);
            my $to = $F{'chfieldto'};
            if (defined $to) {
                $to = trim($to);
                if ($to ne "" && $to !~ /^now$/i) {
                    push(@specialchart, ["creation_ts", "lessthan",
                                         SqlifyDate($to)]);
                }
            }
        }
    }



    if (defined $ref && 0 < @$ref) {
        push(@supptables, "bugs_activity actcheck");

        my @list;
        foreach my $f (@$ref) {
            push(@list, "\nactcheck.fieldid = " . GetFieldID($f));
        }
        push(@wherepart, "actcheck.bug_id = bugs.bug_id");
        push(@wherepart, "(" . join(' OR ', @list) . ")");
        push(@wherepart, "actcheck.bug_when >= " .
             SqlQuote(SqlifyDate($F{'chfieldfrom'})));
        my $to = $F{'chfieldto'};
        if (defined $to) {
            $to = trim($to);
            if ($to ne "" && $to !~ /^now$/i) {
                push(@wherepart, "actcheck.bug_when <= " .
                     SqlQuote(SqlifyDate($to)));
            }
        }
        my $value = $F{'chfieldvalue'};
        if (defined $value) {
            $value = trim($value);
            if ($value ne "") {
                push(@wherepart, "actcheck.added = " .
                     SqlQuote($value))
            }
        }
    }


    foreach my $f ("short_desc", "long_desc", "bug_file_loc",
                   "status_whiteboard", "os_details", "version_details") {
        if (defined $F{$f}) {
            my $s = trim($F{$f});
            if ($s ne "") {
                my $n = $f;
                my $q = SqlQuote($s);
                my $type = $F{$f . "_type"};
                push(@specialchart, [$f, $type, $s]);
            }
        }
    }

    my $chartid;
    # $statusid is used by the code that queries for attachment statuses.
    my $statusid = 0;
    my $f;
    my $ff;
    my $t;
    my $q;
    my $v;
    my $term;
    my %funcsbykey;
    my @funcdefs =
        (
         "^(assigned_to|reporter)," => sub {
             push(@supptables, "profiles map_$f");
             push(@wherepart, "bugs.$f = map_$f.userid");
             $f = "map_$f.login_name";
         },
         "^qa_contact," => sub {
             push(@supptables,
                  "LEFT JOIN profiles map_qa_contact ON bugs.qa_contact = map_qa_contact.userid");
             $f = "map_$f.login_name";
         },

         "^cc," => sub {
            push(@supptables, "LEFT JOIN cc cc_$chartid ON bugs.bug_id = cc_$chartid.bug_id");

            push(@supptables, "LEFT JOIN profiles map_cc_$chartid ON cc_$chartid.who = map_cc_$chartid.userid");
            $f = "map_cc_$chartid.login_name";
         },

         "^long_?desc,changedby" => sub {
             my $table = "longdescs_$chartid";
             push(@supptables, "longdescs $table");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             my $id = DBNameToIdAndCheck($v);
             $term = "$table.who = $id";
         },
         "^long_?desc,changedbefore" => sub {
             my $table = "longdescs_$chartid";
             push(@supptables, "longdescs $table");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             $term = "$table.bug_when < " . SqlQuote(SqlifyDate($v));
         },
         "^long_?desc,changedafter" => sub {
             my $table = "longdescs_$chartid";
             push(@supptables, "longdescs $table");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             $term = "$table.bug_when > " . SqlQuote(SqlifyDate($v));
         },
         "^long_?desc," => sub {
             my $table = "longdescs_$chartid";
             push(@supptables, "longdescs $table");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             $f = "$table.thetext";
         },
         "^attachments\..*," => sub {
             my $table = "attachments_$chartid";
             push(@supptables, "attachments $table");
             push(@wherepart, "bugs.bug_id = $table.bug_id");
             $f =~ m/^attachments\.(.*)$/;
             my $field = $1;
             if ($t eq "changedby") {
                 $v = DBNameToIdAndCheck($v);
                 $q = SqlQuote($v);
                 $field = "submitter_id";
                 $t = "equals";
             } elsif ($t eq "changedbefore") {
                 $v = SqlifyDate($v);
                 $q = SqlQuote($v);
                 $field = "creation_ts";
                 $t = "lessthan";
             } elsif ($t eq "changedafter") {
                 $v = SqlifyDate($v);
                 $q = SqlQuote($v);
                 $field = "creation_ts";
                 $t = "greaterthan";
             }
             if ($field eq "ispatch" && $v ne "0" && $v ne "1") {
                 DisplayError("The only legal values for the <em>Attachment is
                               patch</em> field are 0 and 1.");
                 exit;
             }
             if ($field eq "isobsolete" && $v ne "0" && $v ne "1") {
                 DisplayError("The only legal values for the <em>Attachment is
                               obsolete</em> field are 0 and 1.");
                 exit;
             }
             $f = "$table.$field";
         },
         "^attachstatusdefs.name," => sub {
             # The below has Fun with the names for attachment statuses. This
             # isn't needed for changed* queries, so exclude those - the
             # generic stuff will cope
             return if ($t =~ m/^changed/);

             # Searching for "status != 'bar'" wants us to look for an
             # attachment without the 'bar' status, not for an attachment with
             # a status not equal to 'bar' (Which would pick up an attachment
             # with more than one status). We do this by LEFT JOINS, after
             # grabbing the matching attachment status ids.
             # Note that this still won't find bugs with no attachments, since
             # that isn't really what people would expect.

             # First, get the attachment status ids, using the other funcs
             # to match the WHERE term.
             # Note that we need to reverse the negated bits for this to work
             # This somewhat abuses the definitions of the various terms -
             # eg, does 'contains all' mean that the status has to contain all
             # those words, or that all those words must be exact matches to
             # statuses, which must all be on a single attachment, or should
             # the match on the status descriptions be a contains match, too?

             my $inverted = 0;
             if ($t =~ m/not(.*)/) {
                 $t = $1;
                 $inverted = 1;
             }

             $ref = $funcsbykey{",$t"};
             &$ref;
             SendSQL("SELECT id FROM attachstatusdefs WHERE $term");

             my @as_ids;
             while (MoreSQLData()) {
                 push @as_ids, FetchOneColumn();
             }

             # When searching for multiple statuses within a single boolean chart,
             # we want to match each status record separately.  In other words,
             # "status = 'foo' AND status = 'bar'" should match attachments with
             # one status record equal to "foo" and another one equal to "bar",
             # not attachments where the same status record equals both "foo" and
             # "bar" (which is nonsensical).  In order to do this we must add an
             # additional counter to the end of the "attachstatuses" table
             # reference.
             ++$statusid;

             my $attachtable = "attachments_$chartid";
             my $statustable = "attachstatuses_${chartid}_$statusid";

             push(@supptables, "attachments $attachtable");
             my $join = "LEFT JOIN attachstatuses $statustable ON ".
               "($attachtable.attach_id = $statustable.attach_id AND " .
                "$statustable.statusid IN (" . join(",", @as_ids) . "))";
             push(@supptables, $join);
             push(@wherepart, "bugs.bug_id = $attachtable.bug_id");
             if ($inverted) {
                 $term = "$statustable.statusid IS NULL";
             } else {
                 $term = "$statustable.statusid IS NOT NULL";
             }
         },
         "^changedin," => sub {
             $f = "(to_days(now()) - to_days(bugs.delta_ts))";
         },

         "^keywords," => sub {
             GetVersionTable();
             my @list;
             my $table = "keywords_$chartid";
             foreach my $value (split(/[\s,]+/, $v)) {
                 if ($value eq '') {
                     next;
                 }
                 my $id = GetKeywordIdFromName($value);
                 if ($id) {
                     push(@list, "$table.keywordid = $id");
                 }
                 else {
                     my $htmlv = html_quote($v);
                     DisplayError(qq|There is no keyword named <code>$htmlv</code>.
                                     To search for keywords, consult the
                                    <a href="describekeywords.cgi">list of legal keywords</a>.|);
                     exit;
                 }
             }
             my $haveawordterm;
             if (@list) {
                 $haveawordterm = "(" . join(' OR ', @list) . ")";
                 if ($t eq "anywords") {
                     $term = $haveawordterm;
                 } elsif ($t eq "allwords") {
                     $ref = $funcsbykey{",$t"};
                     &$ref;
                     if ($term && $haveawordterm) {
                         $term = "(($term) AND $haveawordterm)";
                     }
                 }
             }
             if ($term) {
                 push(@supptables, "keywords $table");
                 push(@wherepart, "$table.bug_id = bugs.bug_id");
             }
         },

         "^dependson," => sub {
                my $table = "dependson_" . $chartid;
                push(@supptables, "dependencies $table");
                $ff = "$table.$f";
                $ref = $funcsbykey{",$t"};
                &$ref;
                push(@wherepart, "$table.blocked = bugs.bug_id");
         },

         "^blocked," => sub {
                my $table = "blocked_" . $chartid;
                push(@supptables, "dependencies $table");
                $ff = "$table.$f";
                $ref = $funcsbykey{",$t"};
                &$ref;
                push(@wherepart, "$table.dependson = bugs.bug_id");
         },


         ",equals" => sub {
             $term = "$ff = $q";
         },
         ",notequals" => sub {
             $term = "$ff != $q";
         },
         ",casesubstring" => sub {
             $term = "INSTR($ff, $q)";
         },
         ",(substring|substr)" => sub {
             $term = "INSTR(LOWER($ff), " . lc($q) . ")";
         },
         ",notsubstring" => sub {
             $term = "INSTR(LOWER($ff), " . lc($q) . ") = 0";
         },
         ",regexp" => sub {
             $term = "LOWER($ff) REGEXP $q";
         },
         ",notregexp" => sub {
             $term = "LOWER($ff) NOT REGEXP $q";
         },
         ",lessthan" => sub {
             $term = "$ff < $q";
         },
         ",greaterthan" => sub {
             $term = "$ff > $q";
         },
         ",anyexact" => sub {
             my @list;
             foreach my $w (split(/,/, $v)) {
                 if ($w eq "---" && $f !~ /milestone/) {
                     $w = "";
                 }
                 push(@list, "$ff = " . SqlQuote($w));
             }
             $term = join(" OR ", @list);
         },
         ",anywordssubstr" => sub {
             $term = join(" OR ", @{GetByWordListSubstr($ff, $v)});
         },
         ",allwordssubstr" => sub {
             $term = join(" AND ", @{GetByWordListSubstr($ff, $v)});
         },
         ",nowordssubstr" => sub {
             my @list = @{GetByWordListSubstr($ff, $v)};
             if (@list) {
                 $term = "NOT (" . join(" OR ", @list) . ")";
             }
         },
         ",anywords" => sub {
             $term = join(" OR ", @{GetByWordList($ff, $v)});
         },
         ",allwords" => sub {
             $term = join(" AND ", @{GetByWordList($ff, $v)});
         },
         ",nowords" => sub {
             my @list = @{GetByWordList($ff, $v)};
             if (@list) {
                 $term = "NOT (" . join(" OR ", @list) . ")";
             }
         },
         ",changedbefore" => sub {
             my $table = "act_$chartid";
             my $ftable = "fielddefs_$chartid";
             push(@supptables, "bugs_activity $table");
             push(@supptables, "fielddefs $ftable");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             push(@wherepart, "$table.fieldid = $ftable.fieldid");
             $term = "($ftable.name = '$f' AND $table.bug_when < $q)";
         },
         ",changedafter" => sub {
             my $table = "act_$chartid";
             my $ftable = "fielddefs_$chartid";
             push(@supptables, "bugs_activity $table");
             push(@supptables, "fielddefs $ftable");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             push(@wherepart, "$table.fieldid = $ftable.fieldid");
             $term = "($ftable.name = '$f' AND $table.bug_when > $q)";
         },
         ",changedfrom" => sub {
             my $table = "act_$chartid";
             my $ftable = "fielddefs_$chartid";
             push(@supptables, "bugs_activity $table");
             push(@supptables, "fielddefs $ftable");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             push(@wherepart, "$table.fieldid = $ftable.fieldid");
             $term = "($ftable.name = '$f' AND $table.removed = $q)";
         },
         ",changedto" => sub {
             my $table = "act_$chartid";
             my $ftable = "fielddefs_$chartid";
             push(@supptables, "bugs_activity $table");
             push(@supptables, "fielddefs $ftable");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             push(@wherepart, "$table.fieldid = $ftable.fieldid");
             $term = "($ftable.name = '$f' AND $table.added = $q)";
         },
         ",changedby" => sub {
             my $table = "act_$chartid";
             my $ftable = "fielddefs_$chartid";
             push(@supptables, "bugs_activity $table");
             push(@supptables, "fielddefs $ftable");
             push(@wherepart, "$table.bug_id = bugs.bug_id");
             push(@wherepart, "$table.fieldid = $ftable.fieldid");
             my $id = DBNameToIdAndCheck($v);
             $term = "($ftable.name = '$f' AND $table.who = $id)";
         },
         );
    my @funcnames;
    while (@funcdefs) {
        my $key = shift(@funcdefs);
        my $value = shift(@funcdefs);
        if ($key =~ /^[^,]*$/) {
            die "All defs in %funcs must have a comma in their name: $key";
        }
        if (exists $funcsbykey{$key}) {
            die "Duplicate key in %funcs: $key";
        }
        $funcsbykey{$key} = $value;
        push(@funcnames, $key);
    }

    if ($debug) {
        # If we're going to be printing debug stuff, we need the content
        # type. This doesn't affect later stuff, because we call exit, and
        # never return from this sub
        print "Content-Type: text/html\n\n";
    }

    # first we delete any sign of "Chart #-1" from the HTML form hash
    # since we want to guarantee the user didn't hide something here
    my @badcharts = grep /^(field|type|value)-1-/, (keys %F);
    foreach my $field (@badcharts) {
        delete $F{$field};
    }

    # now we take our special chart and stuff it into the form hash
    my $chart = -1;
    my $row = 0;
    foreach my $ref (@specialchart) {
        my $col = 0;
        while (@$ref) {
            $F{"field$chart-$row-$col"} = shift(@$ref);
            $F{"type$chart-$row-$col"} = shift(@$ref);
            $F{"value$chart-$row-$col"} = shift(@$ref);
            if ($debug) {
                print qq{<P>$F{"field$chart-$row-$col"} | $F{"type$chart-$row-$col"} | $F{"value$chart-$row-$col"}*\n};
            }
            $col++;

        }
        $row++;
    }


# A boolean chart is a way of representing the terms in a logical
# expression.  Bugzilla builds SQL queries depending on how you enter
# terms into the boolean chart. Boolean charts are represented in
# urls as tree-tuples of (chart id, row, column). The query form
# (query.cgi) may contain an arbitrary number of boolean charts where
# each chart represents a clause in a SQL query.
#
# The query form starts out with one boolean chart containing one
# row and one column.  Extra rows can be created by pressing the
# AND button at the bottom of the chart.  Extra columns are created
# by pressing the OR button at the right end of the chart. Extra
# charts are created by pressing "Add another boolean chart".
#
# Each chart consists of an artibrary number of rows and columns.
# The terms within a row are ORed together. The expressions represented
# by each row are ANDed together. The expressions represented by each
# chart are ANDed together.
#
#        ----------------------
#        | col2 | col2 | col3 |
# --------------|------|------|
# | row1 |  a1  |  a2  |      |
# |------|------|------|------|  => ((a1 OR a2) AND (b1 OR b2 OR b3) AND (c1))
# | row2 |  b1  |  b2  |  b3  |
# |------|------|------|------|
# | row3 |  c1  |      |      |
# -----------------------------
#
#        --------
#        | col2 |
# --------------|
# | row1 |  d1  | => (d1)
# ---------------
#
# Together, these two charts represent a SQL expression like this
# SELECT blah FROM blah WHERE ( (a1 OR a2)AND(b1 OR b2 OR b3)AND(c1)) AND (d1)
#
# The terms within a single row of a boolean chart are all constraints
# on a single piece of data.  If you're looking for a bug that has two
# different people cc'd on it, then you need to use two boolean charts.
# This will find bugs with one CC mathing 'foo@blah.org' and and another
# CC matching 'bar@blah.org'.
#
# --------------------------------------------------------------
# CC    | equal to
# foo@blah.org
# --------------------------------------------------------------
# CC    | equal to
# bar@blah.org
#
# If you try to do this query by pressing the AND button in the
# original boolean chart then what you'll get is an expression that
# looks for a single CC where the login name is both "foo@blah.org",
# and "bar@blah.org". This is impossible.
#
# --------------------------------------------------------------
# CC    | equal to
# foo@blah.org
# AND
# CC    | equal to
# bar@blah.org
# --------------------------------------------------------------

# $chartid is the number of the current chart whose SQL we're contructing
# $row is the current row of the current chart

# names for table aliases are constructed using $chartid and $row
#   SELECT blah  FROM $table "$table_$chartid_$row" WHERE ....

# $f  = field of table in bug db (e.g. bug_id, reporter, etc)
# $ff = qualified field name (field name prefixed by table)
#       e.g. bugs_activity.bug_id
# $t  = type of query. e.g. "equal to", "changed after", case sensitive substr"
# $v  = value - value the user typed in to the form
# $q  = sanitized version of user input (SqlQuote($v))
# @supptables = Tables and/or table aliases used in query
# %suppseen   = A hash used to store all the tables in supptables to weed
#               out duplicates.
# $suppstring = String which is pasted into query containing all table names

    # get a list of field names to verify the user-submitted chart fields against
    my %chartfields;
    SendSQL("SELECT name FROM fielddefs");
    while (MoreSQLData()) {
        my ($name) = FetchSQLData();
        $chartfields{$name} = 1;
    }

    $row = 0;
    for ($chart=-1 ;
         $chart < 0 || exists $F{"field$chart-0-0"} ;
         $chart++) {
        $chartid = $chart >= 0 ? $chart : "";
        for ($row = 0 ;
             exists $F{"field$chart-$row-0"} ;
             $row++) {
            my @orlist;
            for (my $col = 0 ;
                 exists $F{"field$chart-$row-$col"} ;
                 $col++) {
                $f = $F{"field$chart-$row-$col"} || "noop";
                $t = $F{"type$chart-$row-$col"} || "noop";
                $v = $F{"value$chart-$row-$col"};
                $v = "" if !defined $v;
                $v = trim($v);
                if ($f eq "noop" || $t eq "noop" || $v eq "") {
                    next;
                }
                # chart -1 is generated by other code above, not from the user-
                # submitted form, so we'll blindly accept any values in chart -1
                if ((!$chartfields{$f}) && ($chart != -1)) {
                    my $errstr = "Can't use " . html_quote($f) . " as a field name.  " .
                        "If you think you're getting this in error, please copy the " .
                        "entire URL out of the address bar at the top of your browser " .
                        "window and email it to <109679\@bugzilla.org>";
                    die "Internal error: $errstr" if $chart < 0;
                    return Error($errstr);
                }

                # This is either from the internal chart (in which case we
                # already know about it), or it was in %chartfields, so it is
                # a valid field name, which means that its ok.
                trick_taint($f);
                $q = SqlQuote($v);
                my $func;
                $term = undef;
                foreach my $key (@funcnames) {
                    if ("$f,$t" =~ m/$key/) {
                        my $ref = $funcsbykey{$key};
                        if ($debug) {
                            print "<P>$key ($f , $t ) => ";
                        }
                        $ff = $f;
                        if (defined ($::legal_customfields{$f})) {
                            my $cf_crc = $chartid;
                            # Use the custom field value
                            $ff = "bcf$cf_crc.value";
                            # Add custom field tables to join
                            push @supptables, "bugs_customfields bcf$cf_crc";
                            push @supptables, "customfields cf$cf_crc";
                            # Add join criteria and custom field name to where clause
                            push @andlist, "bugs.bug_id = bcf$cf_crc.bug_id";
                            push @andlist, "bcf$cf_crc.cf_id = cf$cf_crc.id";
                            push @andlist, "cf$cf_crc.name = '$f'";
                        }
                        elsif ($f !~ /\./) {
                            $ff = "bugs.$f";
                        }
                        &$ref;
                        if ($debug) {
                            print "$f , $t , $term";
                        }
                        if ($term) {
                            last;
                        }
                    }
                }
                if ($term) {
                    push(@orlist, $term);
                }
                else {
                    my $errstr =
                      qq|Cannot seem to handle <code>$F{"field$chart-$row-$col"}</code>
                         and <code>$F{"type$chart-$row-$col"}</code> together|;
                    $chart < 0 ? die "Internal error: $errstr"
                               : DisplayError($errstr) && exit;
                }
            }
            if (@orlist) {
                push(@andlist, "(" . join(" OR ", @orlist) . ")");
            }
        }
    }
    my %suppseen = ("bugs" => 1);
    my $suppstring = "bugs";
    foreach my $str (@supptables) {
        if (!$suppseen{$str}) {
            if ($str !~ /^(LEFT|INNER) JOIN/i) {
                $suppstring .= ",";
            }
            $suppstring .= " $str";
            $suppseen{$str} = 1;
        }
    }
    my $group = "bugs.bug_id" . 
         (@custom_columns > 0 ? ', bugs_customfields.cf_id' : '');
    my $query =  ("SELECT " . join(', ', @fields) .
                  " FROM $suppstring" .
                  " WHERE " . join(' AND ', (@wherepart, @andlist)) .
                  " GROUP BY " . $group);
    $query = SelectVisible($query, $::userid, $::usergroupset);

    if ($debug) {
        print "<P><CODE>" . value_quote($query) . "</CODE><P>\n";
        exit;
    }
    return $query;
=======
# Generate a reasonable filename for the user agent to suggest to the user
# when the user saves the bug list.  Uses the name of the remembered query
# if available.  We have to do this now, even though we return HTTP headers 
# at the end, because the fact that there is a remembered query gets 
# forgotten in the process of retrieving it.
my @time = localtime(time());
my $date = sprintf "%04d-%02d-%02d", 1900+$time[5],$time[4]+1,$time[3];
my $filename = "bugs-$date.$format->{extension}";
if ($::FORM{'cmdtype'} eq "dorem" && $::FORM{'remaction'} =~ /^run/) {
    $filename = "$::FORM{'namedcmd'}-$date.$format->{extension}";
    # Remove white-space from the filename so the user cannot tamper
    # with the HTTP headers.
    $filename =~ s/\s/_/g;
>>>>>>> 1.1.1.2
}

# Take appropriate action based on user's request.
if ($::FORM{'cmdtype'} eq "dorem") {  
    if ($::FORM{'remaction'} eq "run") {
        $::buffer = LookupNamedQuery($::FORM{"namedcmd"});
        $vars->{'searchname'} = $::FORM{'namedcmd'};
        $vars->{'searchtype'} = "saved";
        $params = new Bugzilla::CGI($::buffer);
        $order = $params->param('order') || $order;
    }
    elsif ($::FORM{'remaction'} eq "runseries") {
        $::buffer = LookupSeries($::FORM{"series_id"});
        $vars->{'searchname'} = $::FORM{'namedcmd'};
        $vars->{'searchtype'} = "series";
        $params = new Bugzilla::CGI($::buffer);
        $order = $params->param('order') || $order;
    }
    elsif ($::FORM{'remaction'} eq "forget") {
        confirm_login();
        my $userid = DBNameToIdAndCheck($::COOKIE{"Bugzilla_login"});
        my $qname = SqlQuote($::FORM{'namedcmd'});
        SendSQL("DELETE FROM namedqueries WHERE userid = $userid AND name = $qname");

        # Now reset the cached queries
        Bugzilla->user->flush_queries_cache();

        print $cgi->header();
        # Generate and return the UI (HTML page) from the appropriate template.
        $vars->{'message'} = "buglist_query_gone";
        $vars->{'namedcmd'} = $::FORM{'namedcmd'};
        $vars->{'url'} = "query.cgi";
        $template->process("global/message.html.tmpl", $vars)
          || ThrowTemplateError($template->error());
        exit;
    }
}
elsif (($::FORM{'cmdtype'} eq "doit") && $::FORM{'remtype'}) {
    if ($::FORM{'remtype'} eq "asdefault") {
        confirm_login();
        my $userid = DBNameToIdAndCheck($::COOKIE{"Bugzilla_login"});
        my $qname = SqlQuote($::defaultqueryname);
        my $qbuffer = SqlQuote($::buffer);
        SendSQL("REPLACE INTO namedqueries (userid, name, query)
                 VALUES ($userid, $qname, $qbuffer)");
                 
        $vars->{'message'} = "buglist_new_default_query";
    }
    elsif ($::FORM{'remtype'} eq "asnamed") {
        confirm_login();
        my $userid = DBNameToIdAndCheck($::COOKIE{"Bugzilla_login"});

        my $name = trim($::FORM{'newqueryname'});
        $name || ThrowUserError("query_name_missing");
        $name !~ /[<>&]/ || ThrowUserError("illegal_query_name");
        my $qname = SqlQuote($name);

        my $qbuffer = SqlQuote($::FORM{'newquery'});

        my $tofooter = 1;

        $vars->{'message'} = "buglist_new_named_query";

        # We want to display the correct message. Check if it existed before
        # we insert, because ->queries may fetch from the db anyway
        if (grep { $_->{name} eq $name } @{Bugzilla->user->queries()}) {
            $vars->{'message'} = "buglist_updated_named_query";
        }

        SendSQL("SELECT query FROM namedqueries WHERE userid = $userid AND name = $qname");
        if (FetchOneColumn()) {
            SendSQL("UPDATE  namedqueries
                        SET  query = $qbuffer , linkinfooter = $tofooter
                      WHERE  userid = $userid AND name = $qname");
        }
        else {
            SendSQL("REPLACE INTO namedqueries (userid, name, query, linkinfooter)
                     VALUES ($userid, $qname, $qbuffer, $tofooter)");
        }

        # Make sure to invalidate any cached query data, so that the footer is
        # correctly displayed
        Bugzilla->user->flush_queries_cache();

        $vars->{'queryname'} = $name;
        
        print "Content-Type: text/html\n\n";
        $template->process("global/message.html.tmpl", $vars)
          || ThrowTemplateError($template->error());
        exit;
    }
}


################################################################################
# Column Definition
################################################################################

# Define the columns that can be selected in a query and/or displayed in a bug
# list.  Column records include the following fields:
#
# 1. ID: a unique identifier by which the column is referred in code;
#
# 2. Name: The name of the column in the database (may also be an expression
#          that returns the value of the column);
#
# 3. Title: The title of the column as displayed to users.
# 
# Note: There are a few hacks in the code that deviate from these definitions.
#       In particular, when the list is sorted by the "votes" field the word 
#       "DESC" is added to the end of the field to sort in descending order, 
#       and the redundant short_desc column is removed when the client
#       requests "all" columns.

my $columns = {};
sub DefineColumn {
    my ($id, $name, $title) = @_;
    $columns->{$id} = { 'name' => $name , 'title' => $title };
}

# Column:     ID                    Name                           Title
DefineColumn("bug_id"            , "bugs.bug_id"                , "ID"               );
DefineColumn("alias"             , "bugs.alias"                 , "Alias"           );
DefineColumn("opendate"          , "bugs.creation_ts"           , "Opened"           );
DefineColumn("changeddate"       , "bugs.delta_ts"              , "Changed"          );
DefineColumn("bug_severity"      , "bugs.bug_severity"          , "Severity"         );
DefineColumn("priority"          , "bugs.priority"              , "Priority"         );
DefineColumn("rep_platform"      , "bugs.rep_platform"          , "Hardware"         );
DefineColumn("assigned_to"       , "map_assigned_to.login_name" , "Assignee"         );
DefineColumn("assigned_to_realname", "map_assigned_to.realname" , "Assignee"         );
DefineColumn("reporter"          , "map_reporter.login_name"    , "Reporter"         );
DefineColumn("reporter_realname" , "map_reporter.realname"      , "Reporter"         );
DefineColumn("qa_contact"        , "map_qa_contact.login_name"  , "QA Contact"       );
DefineColumn("qa_contact_realname", "map_qa_contact.realname"   , "QA Contact"       );
DefineColumn("bug_status"        , "bugs.bug_status"            , "Status"           );
DefineColumn("resolution"        , "bugs.resolution"            , "Result"           );
DefineColumn("short_short_desc"  , "bugs.short_desc"            , "Summary"          );
DefineColumn("short_desc"        , "bugs.short_desc"            , "Summary"          );
DefineColumn("status_whiteboard" , "bugs.status_whiteboard"     , "Status Summary"   );
DefineColumn("component"         , "map_components.name"        , "Component"        );
DefineColumn("product"           , "map_products.name"          , "Product"          );
DefineColumn("version"           , "bugs.version"               , "Version"          );
DefineColumn("op_sys"            , "bugs.op_sys"                , "OS"               );
DefineColumn("target_milestone"  , "bugs.target_milestone"      , "Target Milestone" );
DefineColumn("votes"             , "bugs.votes"                 , "Votes"            );
DefineColumn("keywords"          , "bugs.keywords"              , "Keywords"         );
<<<<<<< buglist.cgi
for my $name (keys %::legal_customfields) {
DefineColumn($name, "bugs_customfields.value", $name);
}
=======
DefineColumn("estimated_time"    , "bugs.estimated_time"        , "Estimated Hours"  );
DefineColumn("remaining_time"    , "bugs.remaining_time"        , "Remaining Hours"  );
DefineColumn("actual_time"       , "(SUM(ldtime.work_time)*COUNT(DISTINCT ldtime.bug_when)/COUNT(bugs.bug_id)) AS actual_time", "Actual Hours");
DefineColumn("percentage_complete","(100*((SUM(ldtime.work_time)*COUNT(DISTINCT ldtime.bug_when)/COUNT(bugs.bug_id))/((SUM(ldtime.work_time)*COUNT(DISTINCT ldtime.bug_when)/COUNT(bugs.bug_id))+bugs.remaining_time))) AS percentage_complete", "% Complete"); 
>>>>>>> 1.1.1.2
################################################################################
# Display Column Determination
################################################################################

# Determine the columns that will be displayed in the bug list via the 
# columnlist CGI parameter, the user's preferences, or the default.

my @custom_columns = ();
my %custom_map = ();
my @displaycolumns = ();
if (defined $params->param('columnlist')) {
    if ($params->param('columnlist') eq "all") {
        # If the value of the CGI parameter is "all", display all columns,
        # but remove the redundant "short_desc" column.
        @displaycolumns = grep($_ ne 'short_desc', keys(%$columns));
    }
    else {
        @displaycolumns = split(/[ ,]+/, $params->param('columnlist'));
    }
}
elsif (defined $::COOKIE{'COLUMNLIST'}) {
    # 2002-10-31 Rename column names (see bug 176461)
    my $columnlist = $::COOKIE{'COLUMNLIST'};
    $columnlist =~ s/\bowner\b/assigned_to/;
    $columnlist =~ s/\bowner_realname\b/assigned_to_realname/;
    $columnlist =~ s/\bplatform\b/rep_platform/;
    $columnlist =~ s/\bseverity\b/bug_severity/;
    $columnlist =~ s/\bstatus\b/bug_status/;
    $columnlist =~ s/\bsummaryfull\b/short_desc/;
    $columnlist =~ s/\bsummary\b/short_short_desc/;

    # Use the columns listed in the user's preferences.
    @displaycolumns = split(/ /, $columnlist);
}
else {
    # Use the default list of columns.
    @displaycolumns = @::default_column_list;
}

# remove custom columns from @displaycolumns and add them to @custom_columns
my @displaycolumns_tmp;

foreach my $c (@displaycolumns) {
    if (exists $::legal_customfields{$c}) {
    #  $custom_map{$::legal_customfields{$c}[2]} = scalar @custom_columns;
         push(@custom_columns, $c);
    }
    else {
    push (@displaycolumns_tmp, $c);    
    }
}

@displaycolumns = @displaycolumns_tmp;

# Weed out columns that don't actually exist to prevent the user 
# from hacking their column list cookie to grab data to which they 
# should not have access.  Detaint the data along the way.
@displaycolumns = grep($columns->{$_} && trick_taint($_), @displaycolumns);

# Remove the "ID" column from the list because bug IDs are always displayed
# and are hard-coded into the display templates.
@displaycolumns = grep($_ ne 'bug_id', @displaycolumns);

# Add the votes column to the list of columns to be displayed
# in the bug list if the user is searching for bugs with a certain
# number of votes and the votes column is not already on the list.

# Some versions of perl will taint 'votes' if this is done as a single
# statement, because the votes param is tainted at this point
my $votes = $params->param('votes');
$votes ||= "";
if (trim($votes) && !grep($_ eq 'votes', @displaycolumns)) {
    push(@displaycolumns, 'votes');
}

# Remove the timetracking columns if they are not a part of the group
# (happens if a user had access to time tracking and it was revoked/disabled)
if (!UserInGroup(Param("timetrackinggroup"))) {
   @displaycolumns = grep($_ ne 'estimated_time', @displaycolumns);
   @displaycolumns = grep($_ ne 'remaining_time', @displaycolumns);
   @displaycolumns = grep($_ ne 'actual_time', @displaycolumns);
   @displaycolumns = grep($_ ne 'percentage_complete', @displaycolumns);
}


################################################################################
# Select Column Determination
################################################################################

# Generate the list of columns that will be selected in the SQL query.

# The bug ID is always selected because bug IDs are always displayed.
# Severity, priority, resolution and status are required for buglist
# CSS classes.
my @selectcolumns = ("bug_id", "bug_severity", "priority", "bug_status",
                     "resolution");

# remaining and actual_time are required for precentage_complete calculation:
if (lsearch(\@displaycolumns, "percentage_complete") >= 0) {
    push (@selectcolumns, "remaining_time");
    push (@selectcolumns, "actual_time");
}

# Display columns are selected because otherwise we could not display them.
push (@selectcolumns, @displaycolumns);

# If the user is editing multiple bugs, we also make sure to select the product
# and status because the values of those fields determine what options the user
# has for modifying the bugs.
if ($dotweak) {
    push(@selectcolumns, "product") if !grep($_ eq 'product', @selectcolumns);
    push(@selectcolumns, "bug_status") if !grep($_ eq 'bug_status', @selectcolumns);
}


################################################################################
# Query Generation
################################################################################

# Convert the list of columns being selected into a list of column names.
my @selectnames = map($columns->{$_}->{'name'}, @selectcolumns);

<<<<<<< buglist.cgi
# Generate the basic SQL query that will be used to generate the bug list.
my $query = GenerateSQL(\@selectnames, undef, undef, $::buffer,\@custom_columns);

# Add the customfields columns to the displayed columns
push (@displaycolumns, @custom_columns);


=======
# Remove columns with no names, such as percentage_complete
#  (or a removed *_time column due to permissions)
@selectnames = grep($_ ne '', @selectnames);
>>>>>>> 1.1.1.2

################################################################################
# Sort Order Determination
################################################################################

# Add to the query some instructions for sorting the bug list.
if ($::COOKIE{'LASTORDER'} && (!$order || $order =~ /^reuse/i)) {
    $order = $::COOKIE{'LASTORDER'};
    $order_from_cookie = 1;
}

my $db_order = "";  # Modified version of $order for use with SQL query
if ($order) {

    # Convert the value of the "order" form field into a list of columns
    # by which to sort the results.
    ORDER: for ($order) {
        /\./ && do {
            my @columnnames = map($columns->{lc($_)}->{'name'}, keys(%$columns));
            # A custom list of columns.  Make sure each column is valid.
            foreach my $fragment (split(/,/, $order)) {
                $fragment = trim($fragment);
                # Accept an order fragment matching a column name, with
                # asc|desc optionally following (to specify the direction)
                if (!grep($fragment =~ /^\Q$_\E(\s+(asc|desc))?$/, @columnnames)) {
                    my $vars = { fragment => $fragment };
                    if ($order_from_cookie) {
                        $cgi->send_cookie(-name => 'LASTORDER',
                                          -expires => 'Tue, 15-Sep-1998 21:49:00 GMT');
                        ThrowCodeError("invalid_column_name_cookie", $vars);
                    }
                    else {
                        ThrowCodeError("invalid_column_name_form", $vars);
                    }
                }
            }
            # Now that we have checked that all columns in the order are valid,
            # detaint the order string.
            trick_taint($order);
            last ORDER;
        };
        /Number/ && do {
            $order = "bugs.bug_id";
            last ORDER;
        };
        /Import/ && do {
            $order = "bugs.priority, bugs.bug_severity";
            last ORDER;
        };
        /Assign/ && do {
            $order = "map_assigned_to.login_name, bugs.bug_status, bugs.priority, bugs.bug_id";
            last ORDER;
        };
        /Changed/ && do {
            $order = "bugs.delta_ts, bugs.bug_status, bugs.priority, map_assigned_to.login_name, bugs.bug_id";
            last ORDER;
        };
        # DEFAULT
        $order = "bugs.bug_status, bugs.priority, map_assigned_to.login_name, bugs.bug_id";
        }
     if (@custom_columns > 1) {
         $order =~ s/bugs_customfields.value,//g;
    }
    foreach my $fragment (split(/,/, $order)) {
        $fragment = trim($fragment);
        if (!grep($fragment =~ /^\Q$_\E(\s+(asc|desc))?$/, @selectnames)) {
            # Add order columns to selectnames
            # The fragment has already been validated
            $fragment =~ s/\s+(asc|desc)$//;
            $fragment =~ tr/a-zA-Z\.0-9\-_//cd;
            push @selectnames, $fragment;
        }
    }

    $db_order = $order;  # Copy $order into $db_order for use with SQL query

    # If we are sorting by votes, sort in descending order if no explicit
    # sort order was given
    $db_order =~ s/bugs.votes\s*(,|$)/bugs.votes desc$1/i;

    # the 'actual_time' field is defined as an aggregate function, but 
    # for order we just need the column name 'actual_time'
    my $aggregate_search = quotemeta($columns->{'actual_time'}->{'name'});
    $db_order =~ s/$aggregate_search/actual_time/g;

    # the 'percentage_complete' field is defined as an aggregate too
    $aggregate_search = quotemeta($columns->{'percentage_complete'}->{'name'});
    $db_order =~ s/$aggregate_search/percentage_complete/g;

}

# Generate the basic SQL query that will be used to generate the bug list.
my $search = new Bugzilla::Search('fields' => \@selectnames, 
                                  'params' => $params);
my $query = $search->getSQL();

# Extra special disgusting hack: if we are ordering by target_milestone,
# change it to order by the sortkey of the target_milestone first.
if ($db_order =~ /bugs.target_milestone/) {
    $db_order =~ s/bugs.target_milestone/ms_order.sortkey,ms_order.value/;
    $query =~ s/\sWHERE\s/ LEFT JOIN milestones ms_order ON ms_order.value = bugs.target_milestone AND ms_order.product_id = bugs.product_id WHERE /;
}

# Even more disgusting hack: if we are doing a full text search,
# order by relevance instead of anything else, and limit to 200 results.
if ($search->{'sorted_by_relevance'}) {
    $db_order = $order = "relevance DESC LIMIT 200";
    $vars->{'sorted_by_relevance'} = 1;
}



$query .= " ORDER BY $db_order " if ($order);


################################################################################
# Query Execution
################################################################################

if ($::FORM{'debug'}) {
    $vars->{'debug'} = 1;
    $vars->{'query'} = $query;
}

# Time to use server push to display an interim message to the user until
# the query completes and we can display the bug list.
if ($serverpush) {
    print $cgi->multipart_init(-content_disposition => "inline; filename=$filename");

    print $cgi->multipart_start();

    # Generate and return the UI (HTML page) from the appropriate template.
    $template->process("list/server-push.html.tmpl", $vars)
      || ThrowTemplateError($template->error());

    print $cgi->multipart_end();
}

# Connect to the shadow database if this installation is using one to improve
# query performance.
Bugzilla->switch_to_shadow_db();

# Normally, we ignore SIGTERM and SIGPIPE (see globals.pl) but we need to
# respond to them here to prevent someone DOSing us by reloading a query
# a large number of times.
$::SIG{TERM} = 'DEFAULT';
$::SIG{PIPE} = 'DEFAULT';

# Execute the query.
SendSQL($query);


################################################################################
# Results Retrieval
################################################################################

# Retrieve the query results one row at a time and write the data into a list
# of Perl records.

my $bugowners = {};
my $bugproducts = {};
my $bugstatuses = {};
my @bugidlist;

my @bugs; # the list of records
my $bug; # a record
my $previousID;

# delete the ID field.
shift @selectcolumns;

# Completely stupid : we get a complete record for each customfield 
# required in the list (2 customfields = 2 complete records taken from the base)
# The SQL request should be improved to change that.

while (my @row = FetchSQLData()) {
    
    my $currentID = shift @row;

    if (defined $previousID && ($currentID ne $previousID)) {
       push @bugs, $bug;
       $bug = {};
    }
    
    # Slurp the row of data into the record.
    # The second from last column in the record is the number of groups
    # to which the bug is restricted.
    foreach my $column (@selectcolumns) {
        $bug->{$column} = shift @row;
    }
    
    # Get the customfields values 
#    my $customfieldName = shift @row;
#    $bug->{$customfieldName}= shift @row;    
   
    # Process certain values further (i.e. date format conversion).
    if ($bug->{'changeddate'}) {
        $bug->{'changeddate'} =~ 
          s/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/$1-$2-$3 $4:$5:$6/;
        $bug->{'changeddate'} = DiffDate($bug->{'changeddate'});
    }
    ($bug->{'opendate'} = DiffDate($bug->{'opendate'})) if $bug->{'opendate'};

    # Record the owner, product, and status in the big hashes of those things.
    $bugowners->{$bug->{'assigned_to'}} = 1 if $bug->{'assigned_to'};
    $bugproducts->{$bug->{'product'}} = 1 if $bug->{'product'};
    $bugstatuses->{$bug->{'bug_status'}} = 1 if $bug->{'bug_status'};

    $bug->{isingroups} = 0;

    # Add the record to the list.
<<<<<<< buglist.cgi
    $bug->{'id'} = $currentID ;
    $previousID = $currentID;
}

if ($previousID) {
	push @bugs, $bug;
=======
    push(@bugs, $bug);

    # Add id to list for checking for bug privacy later
    push(@bugidlist, $bug->{'bug_id'});
>>>>>>> 1.1.1.2
}

# Check for bug privacy and set $bug->{isingroups} = 1 if private 
# to 1 or more groups
my %privatebugs;
if (@bugidlist) {
    SendSQL("SELECT DISTINCT bugs.bug_id FROM bugs, bug_group_map " .
            "WHERE bugs.bug_id = bug_group_map.bug_id " .
            "AND bugs.bug_id IN (" . join(',',@bugidlist) . ")");
    while (MoreSQLData()) {
        my ($bug_id) = FetchSQLData();
        $privatebugs{$bug_id} = 1;
    }
    foreach my $bug (@bugs) {
        if ($privatebugs{$bug->{'bug_id'}}) {
            $bug->{isingroups} = 1;
        }
    }
}

################################################################################
# Template Variable Definition
################################################################################

# Define the variables and functions that will be passed to the UI template.

$vars->{'bugs'} = \@bugs;
$vars->{'buglist'} = join(',', @bugidlist);
$vars->{'columns'} = $columns;
$vars->{'displaycolumns'} = \@displaycolumns;

my @openstates = OpenStates();
$vars->{'openstates'} = \@openstates;
$vars->{'closedstates'} = ['CLOSED', 'VERIFIED', 'RESOLVED'];

# The list of query fields in URL query string format, used when creating
# URLs to the same query results page with different parameters (such as
# a different sort order or when taking some action on the set of query
# results).  To get this string, we start with the raw URL query string
# buffer that was created when we initially parsed the URL on script startup,
# then we remove all non-query fields from it, f.e. the sort order (order)
# and command type (cmdtype) fields.
$vars->{'urlquerypart'} = $::buffer;
$vars->{'urlquerypart'} =~ s/(order|cmdtype)=[^&]*&?//g;
$vars->{'order'} = $order;

# The user's login account name (i.e. email address).
my $login = $::COOKIE{'Bugzilla_login'};

$vars->{'caneditbugs'} = UserInGroup('editbugs');

# Whether or not this user is authorized to move bugs to another installation.
$vars->{'ismover'} = 1
  if Param('move-enabled')
    && defined($login)
      && Param('movers') =~ /^(\Q$login\E[,\s])|([,\s]\Q$login\E[,\s]+)/;

my @bugowners = keys %$bugowners;
if (scalar(@bugowners) > 1 && UserInGroup('editbugs')) {
    my $suffix = Param('emailsuffix');
    map(s/$/$suffix/, @bugowners) if $suffix;
    my $bugowners = join(",", @bugowners);
    $vars->{'bugowners'} = $bugowners;
}

# Whether or not to split the column titles across two rows to make
# the list more compact.
$vars->{'splitheader'} = $::COOKIE{'SPLITHEADER'} ? 1 : 0;

$vars->{'quip'} = GetQuip();
$vars->{'currenttime'} = time();

# The following variables are used when the user is making changes to multiple bugs.
if ($dotweak) {
    $vars->{'dotweak'} = 1;
    $vars->{'use_keywords'} = 1 if @::legal_keywords;

    $vars->{'products'} = \@::legal_product;
    $vars->{'platforms'} = \@::legal_platform;
    $vars->{'priorities'} = \@::legal_priority;
    $vars->{'severities'} = \@::legal_severity;
    $vars->{'resolutions'} = \@::settable_resolution;

    $vars->{'unconfirmedstate'} = $::unconfirmedstate;

    $vars->{'bugstatuses'} = [ keys %$bugstatuses ];

    # The groups to which the user belongs.
    $vars->{'groups'} = GetGroupsByUserId($::userid);

    # If all bugs being changed are in the same product, the user can change
    # their version and component, so generate a list of products, a list of
    # versions for the product (if there is only one product on the list of
    # products), and a list of components for the product.
    $vars->{'bugproducts'} = [ keys %$bugproducts ];
    if (scalar(@{$vars->{'bugproducts'}}) == 1) {
        my $product = $vars->{'bugproducts'}->[0];
        $vars->{'versions'} = $::versions{$product};
        $vars->{'components'} = $::components{$product};
        $vars->{'targetmilestones'} = $::target_milestone{$product} if Param('usetargetmilestone');
    }
}


################################################################################
# HTTP Header Generation
################################################################################

# Generate HTTP headers

my $contenttype;

if ($format->{'extension'} eq "html") {
    my $cookiepath = Param("cookiepath");

    if ($order) {
        $cgi->send_cookie(-name => 'LASTORDER',
                          -value => $order,
                          -expires => 'Fri, 01-Jan-2038 00:00:00 GMT');
    }
    my $bugids = join(":", @bugidlist);
    # See also Bug 111999
    if (length($bugids) < 4000) {
        $cgi->send_cookie(-name => 'BUGLIST',
                          -value => $bugids,
                          -expires => 'Fri, 01-Jan-2038 00:00:00 GMT');
    }
    else {
        $cgi->send_cookie(-name => 'BUGLIST',
                          -expires => 'Tue, 15-Sep-1998 21:49:00 GMT');
        $vars->{'toolong'} = 1;
    }

    $contenttype = "text/html";
}
else {
    $contenttype = $format->{'ctype'};
}

if ($serverpush) {
    print $cgi->multipart_start(-type=>$contenttype);
} else {
    # Suggest a name for the bug list if the user wants to save it as a file.
    # If we are doing server push, then we did this already in the HTTP headers
    # that started the server push, so we don't have to do it again here.
    print $cgi->header(-type => $contenttype,
                       -content_disposition => "inline; filename=$filename");
}


################################################################################
# Content Generation
################################################################################

# Generate and return the UI (HTML page) from the appropriate template.
$template->process($format->{'template'}, $vars)
  || ThrowTemplateError($template->error());


################################################################################
# Script Conclusion
################################################################################

print $cgi->multipart_final() if $serverpush;
