#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
#TODO:
#OS should auto-detect-swipe, update b.g.o code, which barfs on galeon, AFAICS
#page three should be a 'these are bugs already reported' [maybe 'recently' reported?]
#do a little 'you didn't select anything' warning

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

# Bugzilla 2.16 no has the make_popup or make_selection_widget
# functions (it uses templates instead).  We really should use
# templates as well, but I'm more interested in having something work
# than having it be perfect.  So, I'm cutting and pasting those
# functions from 2.10's CGI.pl to here:
sub make_popup {
    my ($name,$src,$default,$listtype,$onchange) = (@_);
    my $popup = "<SELECT NAME=$name";
    if ($listtype > 0) {
        $popup .= " SIZE=5";
        if ($listtype == 2) {
            $popup .= " MULTIPLE";
        }
    }
    if (defined $onchange && $onchange ne "") {
        $popup .= " onchange=$onchange";
    }
    $popup .= ">" . make_options($src, $default,
                                 ($listtype == 2 && $default ne ""));
    $popup .= "</SELECT>";
    return $popup;
}

#
# make_selection_widget: creates an HTML selection widget from a list
#   of text strings.
# $groupname is the name of the setting (form value) that this widget
#   will control
# $src is the list of options
# you can specify a $default value which is either a string or a regex
#   pattern to match to identify the default value
# $capitalize lets you optionally capitalize the option strings; the
#   default is 0 (false)
# $multiple is 1 if several options are selectable (default), 0 otherwise.
# $size is used for lists to control how many items are shown. The
#   default is 7. A list of size 1 becomes a popup menu.
# $preferLists is 1 if selection lists should be used in favor of
#   radio buttons and checkboxes, and 0 otherwise. The default is 1 (true)
#
# The actual widget generated depends on the parameter settings:
# 
#    MULTIPLE   PREFERLISTS  SIZE   RESULT
#   0 (single)      0         =1    Popup Menu (normal for list of size 1)
#   0 (single)      0         >1    Radio buttons
#   0 (single)      1         =1    Popup Menu (normal for list of size 1)
#   0 (single)      1         n>1   List of size n, single selection
#   1 (multi)       0         n/a   Check boxes; size ignored
#   1 (multi)       1         n/a   List of size n, multiple selection, of size n
#
sub make_selection_widget {
    my ($groupname,$src,$default,$isregexp,$multiple, $size, $capitalize, $preferLists) = (@_);
    my $last = "";
    my $popup = "";
    my $found = 0;
    my $displaytext = "";
    $groupname = "" if !defined $groupname;
    $default = "" if !defined $default;
    $capitalize = 0 if !defined $capitalize;
    $multiple = 1 if !defined $multiple;
    $preferLists = 1 if !defined $preferLists;
    $size = 7 if !defined $size;
    my $type = "LIST";
    if (!$preferLists) {
        if ($multiple) {
            $type = "CHECKBOX";
        } else {
            if ($size > 1) {
                $type = "RADIO";
            }
        }
    }

    if ($type eq "LIST") {
        $popup .= "<SELECT NAME=\"$groupname\"";
        if ($multiple) {
            $popup .= " MULTIPLE";
        }
        $popup .= " SIZE=$size>\n";
    }
    if ($src) {
        foreach my $item (@$src) {
            if ($item eq "-blank-" || $item ne $last) {
                if ($item eq "-blank-") {
                    $item = "";
                }
                $last = $item;
                $displaytext = $item;
                if ($capitalize) {
                    $displaytext =~ tr/A-Z/a-z/;
                    $displaytext =~ s/^(.?)(.*)/\u$1$2/;
                }   

                if ($isregexp ? $item =~ $default : $default eq $item) {
                    if ($type eq "CHECKBOX") {
                        $popup .= "<INPUT NAME=$groupname type=checkbox VALUE=\"$item\" CHECKED>$displaytext<br>";
                    } elsif ($type eq "RADIO") {
                        $popup .= "<INPUT NAME=$groupname type=radio VALUE=\"$item\" check>$displaytext<br>";
                    } else {
                        $popup .= "<OPTION SELECTED VALUE=\"$item\">$displaytext";
                    }
                    $found = 1;
                } else {
                    if ($type eq "CHECKBOX") {
                        $popup .= "<INPUT NAME=$groupname type=checkbox VALUE=\"$item\">$displaytext<br>";
                    } elsif ($type eq "RADIO") {
                        $popup .= "<INPUT NAME=$groupname type=radio VALUE=\"$item\">$displaytext<br>";
                    } else {
                        $popup .= "<OPTION VALUE=\"$item\">$displaytext";
                    }
                }
            }
        }
    }
    if (!$found && $default ne "") {
        if ($type eq "CHECKBOX") {
            $popup .= "<INPUT NAME=$groupname type=checkbox CHECKED>$default";
        } elsif ($type eq "RADIO") {
            $popup .= "<INPUT NAME=$groupname type=radio checked>$default";
        } else {
            $popup .= "<OPTION SELECTED>$default";
        }
    }
    if ($type eq "LIST") {
        $popup .= "</SELECT>";
    }
    return $popup;
}


#set up the page
ConnectToDatabase(1);
GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

my $product = $::FORM{'product'};
my $component = $::FORM{'component'};
my $search = $::FORM{'search'};

#no product? We're on Page One.
if (!defined $product)
{
PutHeader("Simple GNOME Bug Form: Page 1",'');

#FIXME: we need to autogenerate the product list, maybe?
print<<FIN;
<style type="text/css">
<!--

	table.pretty {
		border-color: #000000;
		border-width: 1pt;
		border-style: solid;
		background-color: #ECECEC;
	}

	td.pretty {
		padding: 20pt;
	}
	
-->
</style>

<body bgcolor="#ffffff" text="#000000">
<center><h1>Submitting a GNOME2 Bug Report</h1>(in just a few simple steps)</center><br>
<center>
A <a href="enter_bug.cgi">more sophisticated bug submission interface</a> is also available.
</center>
<br>

<form method=post action="simple-bug-guide.cgi">

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/one.png" height="64" width="64" alt="Step 1" border="0"></td>
<td class="pretty">
If you have not already done so, <a href="createaccount.cgi">create a Bugzilla account</a> by giving us a valid email address. This is necessary before you do anything else with Bugzilla, but only requires entry of a valid email address.
</td>
</tr>
</table>

<br><br>

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/two.png" height="64" width="64" alt="Step 2" border="0"></td>
<td class="pretty" width="200">
Which product is this bug in?  Select a product from the list on the right.<p>This page is only for core <b>GNOME2 end-user applications</b>- if you're looking for a library, or for a more complete listing of libraries and applications, or to file a bug against a GNOME1.4 version of this page, please use our <a href="enter_bug.cgi">general bug submission form.</a>
</td>
<td>
<select name='product' SIZE=10 onchange=0>
<option VALUE='control-center'>Control Center
<option VALUE='EOG'>Eye of GNOME
<option VALUE='gconf'>gconf
<option VALUE='gconf-editor'>gconf Editor
<option VALUE='gdm'>GNOME2 Display Manager [gdm]
<option VALUE='gedit'>gedit
<option VALUE='GGV'>GNOME2 Ghostscript Viewer [GGV]
<option VALUE='glade'>glade
<option VALUE='gnome-applets'>GNOME2 Applets
<option VALUE='gnome-games'>GNOME2 Games
<option VALUE='gnome-media'>GNOME2 Media Player
<option VALUE='gnome-mime-data'>MIME Database [gnome-mime-data]
<option VALUE='gnome-panel'>GNOME2 Panel
<option VALUE='gnome-session'>GNOME2 Session Management
<option VALUE='gnome-terminal'>GNOME2 Terminal
<option VALUE='gnome-user-docs'>GNOME2 User Docs
<option VALUE='gnome-utils'>GNOME2 Utilities
<option VALUE='metacity'>Metacity Window Manager
<option VALUE='nautilus'>Nautilus
<option VALUE='control-center'>Preferences
<option VALUE='sawfish'>Sawfish Window Manager
</select>
</td>
</tr>
</table>

<br><br>

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty">
<center>Once you're done selecting a product, hit the button.<br><br>
<input type=submit value="Next Page"></center>
</td>
</tr>
</table>

</form>

<br>
FIN
}

#do we have a product but no component? let's get a component list
if((defined $product)&&(!defined $component))
{
PutHeader("Simple GNOME Bug Form: Page Two",'');
#FIXME: we need to grab a component list somehow here
print<<FIN;
<br><br>
<form method=post action="simple-bug-guide.cgi">
<input type=hidden name=product value="$product">

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/three.png" height="64" width="64" alt="Step 3" border="0"></td>
<td class="pretty">
FIN

print "<B>What component of $product is the bug in? <A HREF=\"describecomponents.cgi?product=" . url_quote($product) . "\">A description of the various components is here.</A> If you're not sure, take your best guess.</B></td>\n<td class=\"pretty\">".make_popup('component', $::components{$product}, '', 1, 0);

print<<FIN;
</td>
</tr>
</table>

<br><br>

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty">
<center>Once you've selected a component from the list above, hit the button.<br><br>
<input type="submit" value="Next Page"></center>
</td>
</tr>
</table>

</form>

<br>
FIN
}

#do we have a product and a component? But we haven't done a search yet?
if((defined $product)&&(defined $component)&&(!defined $search))
{
PutHeader("Simple GNOME Bug Form: Page Three",'');

print<<FIN;
<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/four.png" height="64" width="64" alt="Step 4" border="0"></td>
<td class="pretty">
Below is a list of bugs that might match your criteria. Please read through them to make sure your bug has not been previously reported.<br>If your bug has been reported, please cc: yourself to that bug and add any relevant details you know of as a comment, instead of filing a new bug.
</td>
<td class="pretty">
<form method=post action="simple-bug-guide.cgi">
<input type=hidden name=component value="$component">
<input type=hidden name=product value="$product">
<input type=hidden name=search value='done'>
<input type=submit value="I don't see my bug here">
</form>
</td>
</tr>
</table>

<br>
FIN

#query only on product/component
my $query = <<FIN;
select
    bugs.bug_id, bugs.short_desc, bugs.bug_status
from   bugs
where
    (
     bugs.product = '$product'
     and
     bugs.component = '$component'
     and
     bugs.keywords LIKE '%GNOMEVER2%'
)
ORDER BY bug_id DESC
LIMIT 25
FIN

# End build up $query string
    SendSQL ($query);

print<<FIN;
<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty">
<p align="center"><b>List of recently reported GNOME2 bugs against the \'$component\' component of $product</b></p>
<p>
FIN

my $buglist;
    while (my ($bug_id, $short_desc, $status) = FetchSQLData()) {
        print '<a href="show_bug.cgi?id='.$bug_id.'">'.$bug_id.'</A>: '.$short_desc.' ['.$status.']</BR>';
    }

}

print<<FIN;
</p>
</td>
</tr>
</table>

<br>
FIN

#do we have a produce and a component? and we've searched!
if((defined $product)&&(defined $component)&&($search eq 'done'))
{
#we only do this here so as not to scare people off before they've had a chance
confirm_login();
my $login_name = $::COOKIE{"Bugzilla_login"};

PutHeader("Simple GNOME Bug Form: Page Four",'');

print<<FIN;
<p>
<form method="post" action="../post_bug.cgi">
<input type="hidden" name="reporter" value="$login_name">
<input type="hidden" name="version" value="unspecified">
<input type="hidden" name="product" value="$product">
<input type="hidden" name="component" value="$component">
<input type="hidden" name="keywords" value='GNOMEVER2.0'>
<input type="hidden" name="priority" value='Normal'>
<input type="hidden" name="rep_platform" value="Other">
<input type="hidden" name="bug_file_loc" value="">

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/five.png" height="64" width="64" alt="Step 5" border="0"></td>
<td class="pretty">
Describe your bug in a short sentence. Be as descriptive as possible- 'it is broken' is not very useful, while 'the text entry widget won't let me enter text' is much more informative.
</td>
<td class="pretty">
    <ul>
       <input name="short_desc" size=50 value="">
    </ul>
</td>
</tr>
</table>

<br><br>

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/six.png" height="64" width="64" alt="Step 6" border="0"></td>
<td class="pretty">
How bad do you think this bug is?<br>For more detail on this field, you can look <a href="http://bugzilla.gnome.org/bug_status.html#severity">here.</a>
</td>
<td class="pretty">
    <select name=bug_severity>
     <option name="blocker" value="blocker" >
Blocker: The bug creates a security risk or destroys data
     <option name="critical" value="critical">
Critical: The program crashes, hangs, or leaks memory
     <option name="major" value="major">
Major:  A major feature is broken
     <option name="normal" value="normal" SELECTED>
Normal: A minor feature is broken
     <option name="minor" value="minor">
Minor: Minor loss of function, and there is an easy workaround
     <option name="trivial" value="trivial">
Trivial: A cosmetic problem such as a misspelled word
     <option name="enhancement" value="enhancement">
Enhancement: Request for new feature or enhancement.
    </select>
</td>
</tr>
</table>

<br><br>

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/seven.png" height="64" width="64" alt="Step 7" border="0"></td>
<td class="pretty">
Please write a detailed description of the bug. The template is here to help you provide the details we need to reproduce and debug the problem. For more details on what is best to put in this field, check out the <a href="http://bugzilla.gnome.org/gnome-docs/bug-tips.cgi">Bug Dos and Donts</a> page.
    <ul>
       <textarea name=comment rows=15 cols=50 wrap=hard>Description of Problem:


Steps to reproduce the problem:
1. 
2. 
3. 

Actual Results:


Expected Results:


How often does this happen? 


Additional Information:


</textarea>
    </ul>


</td>
</tr>
</table>

<br><br>

<table class="pretty" align="center" width="95%">
<tr>
<td class="pretty" width="64"><img src="../images/eight.png" height="64" width="64" alt="Step 8" border="0"></td>
<td class="pretty">
Which operating system are you running?
</td>
<td class="pretty">
FIN
print make_selection_widget("op_sys", \@::legal_opsys, 'All', '', 1);
print<<FIN;
</td>
</tr>
</table>

<br><br>

<center>
   <input type=submit value="File Bug Report">
</center>

</form>

<br><br>

FIN
}

PutFooter();
