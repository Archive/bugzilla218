#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-

use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

my $product = $::FORM{'product'};
my $component = $::FORM{'component'};

if(!defined($product) || $product eq '')
{
PutHeader("find unreviewed patches",'');
print<<FIN
Enter a product name to get the list of bugs against the product that have outstanding, unreviewed patches.<br><br>
Product Name:<br>
<form action="patch-status.cgi" method="POST">
	<input type="text" name="product">
	<input type="submit" value="Go">
	</form>
Notes:
<ul>
<li><b>BE PATIENT! The query will probably take 5-10 seconds.</b></li>
<li>'unreviewed' here means that no one has used the patch status editor to mark a patch obsolete, needs-work, or otherwise has changed the status of the bug. Use the patch editor to change these statuses.</li>
<li>Please file bugs with specific bug numbers if they should be on this query and aren't, or vice-versa.</li>
<li>To bookmark this page for your product, use patch-status.cgi?product=\$YOURPRODUCTHERE</li>
<li>You can search on product 'all' to get a list of bugs on all products.</li>
</ul>
FIN
}

else{
#here we have an arbitrary SQL query
my $query = <<FIN;
SELECT 
  distinct(attachments.bug_id)
FROM 
  attachments 
LEFT JOIN 
  attachstatuses ON attachments.attach_id=attachstatuses.attach_id 
LEFT JOIN 
  bugs ON attachments.bug_id=bugs.bug_id 
WHERE 
  attachstatuses.statusid IS NULL AND
  attachments.ispatch='1' AND
  attachments.isobsolete!='1' AND
  (
   bugs.bug_status='UNCONFIRMED' OR
   bugs.bug_status='NEW' OR
   bugs.bug_status='ASSIGNED' OR 
   bugs.bug_status='REOPENED'
  )
FIN

if($product !~ /all/i)
{
$query .='AND product = '.SqlQuote($product);
}

if($component ne '')
{
$query .='AND component = '.SqlQuote($component);
}

$query .=';';

# End build up $query string and submit it to the DB
SendSQL ($query);

# here we do a crappy join on the returned data
my $buglist;
    while (my ($bug_id) = FetchSQLData()) {
        $buglist .= $bug_id;
        $buglist .= ',';
    }


# make sure $buglist is not empty
if($buglist eq '')
{
PutHeader("find unreviewed patches",'');
print("No outstanding patches found for product - $product.");
exit
}

# we do a redirect to the relevant list
print<<FIN;
<HEAD>
<meta http-equiv="refresh" content="0; url=http://bugzilla.gnome.org/buglist.cgi?bug_id=$buglist">
</HEAD>
FIN
}

#PutFooter();
